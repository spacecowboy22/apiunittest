<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hastodolist;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $todolist;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Todolists", mappedBy="user_email", cascade={"persist", "remove"})
     */
    private $items;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getHastodolist(): ?bool
    {
        return $this->hastodolist;
    }

    public function setHastodolist(?bool $hastodolist): self
    {
        $this->hastodolist = $hastodolist;

        return $this;
    }

    public function getTodolist(): ?bool
    {
        return $this->todolist;
    }

    public function setTodolist(?bool $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }

    public function getItems(): ?Todolists
    {
        return $this->items;
    }

    public function setItems(?Todolists $items): self
    {
        $this->items = $items;

        // set (or unset) the owning side of the relation if necessary
        $newUser_email = null === $items ? null : $this;
        if ($items->getUserEmail() !== $newUser_email) {
            $items->setUserEmail($newUser_email);
        }

        return $this;
    }

    public function isValidEmail()
    {
        return !empty($this->email) && filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }


    public function isValidFullName()
    {
        return !empty($this->firstname) && !empty($this->lastname) ;

    }

    public function isValidAge()
    {
        return $this->age >= 13;
    }

    public function isValidPassword()
    {
        return !empty($this->password) && strlen($this->password) > 8 && strlen($this->password) < 40 ;
    }

    public function isValid()
    {
        return $this->isValidFullName() && $this->isValidAge() && $this->isValidPassword() && $this->isValidEmail();
    }

    public function hasTodoList()
    {
        return $this->hastodolist;
    }

    public function createTodoList()
    {
        if($this->hasTodoList())
            return false;

        $this->todolist = new Todolist($this);
        $this->hastodolist = true;

        return true;

    }
}
