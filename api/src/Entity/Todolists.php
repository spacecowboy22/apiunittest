<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\TodolistsRepository")
 */
class Todolists
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Users", inversedBy="items", cascade={"persist", "remove"})
     */
    private $user_email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Items", mappedBy="todolist")
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEmail(): ?Users
    {
        return $this->user_email;
    }

    public function setUserEmail(?Users $user_email): self
    {
        $this->user_email = $user_email;

        return $this;
    }

    /**
     * @return Collection|Items[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Items $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setTodolist($this);
        }

        return $this;
    }

    public function removeItem(Items $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getTodolist() === $this) {
                $item->setTodolist(null);
            }
        }

        return $this;
    }

    public function canAddNewItem()
    {
        return count($this->items) >= 0 && count($this->items) < 10 ;
    }


    public function isItemUnique(Items $newItem)
    {
        $result = true;
        foreach ($this->items as $item) {
            if($item->name == $newItem->getName())
                $result = false;
        }

        return $result;
    }

    public function diffTimeItem()
    {
        $currentTime = new DateTime("now");

        if(count($this->items) == 0)
            return true;

        $lastItem = end($this->items);

        $lastItemTime = $lastItem->createdAt;

        $lastItemTime->modify("+30 minutes");

        return $currentTime >= $lastItemTime;

    }


    public function canAddItem(Items $item)
    {
        if($this->canAddNewItem() && $this->isItemUnique($item) && $this->diffTimeItem())
            return $item;

        else
            return null ;
    }
}
