<?php

namespace App\Repository;

use App\Entity\Todolists;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Todolists|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todolists|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todolists[]    findAll()
 * @method Todolists[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodolistsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todolists::class);
    }

    // /**
    //  * @return Todolists[] Returns an array of Todolists objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Todolists
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
